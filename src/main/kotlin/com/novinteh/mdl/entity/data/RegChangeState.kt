package com.novinteh.mdl.entity.data

import com.google.gson.annotations.Expose

/**
 * @author Marinchenko V. A.
 */
enum class RegChangeState(@Expose val state: String) {
    DONE("DONE"),
    CANCELLED("CANCELLED"),
    FAILED("FAILED"),
    LATE("LATE"),
    AWAIT("AWAIT"),
    NOT_SCHEDULED("NOT_SCHEDULED"),
    UNKNOWN("UNKNOWN")
}