package com.novinteh.mdl.entity.data

import com.google.gson.annotations.Expose
import com.novinteh.mdl.entity.base.Jsonable

/**
 * Технологический участок.
 * @author Marinchenko V. A.
 */
data class Tehsect(
        @Expose val tehsectId: Int,
        @Expose val fullName: String?,
        @Expose val shortName: String?
) : Jsonable()