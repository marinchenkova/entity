package com.novinteh.mdl.entity.data

import com.google.gson.annotations.Expose
import com.novinteh.mdl.entity.base.Jsonable
import com.novinteh.mdl.entity.util.formatHHmm
import com.novinteh.mdl.entity.util.hourDiff
import com.novinteh.mdl.entity.util.is24
import com.novinteh.mdl.entity.util.onNextDay
import java.util.*


const val TIME_FAIL_H = 4
const val TIME_LATE_H = 2
const val TIME_NEXT_00 = "24:00"

/**
 * Плановый переход.
 * @author Marinchenko V. A.
 */
data class RegChange(
        @Expose val tehsect: Tehsect,
        private val planDate: Date?,
        private val factDate: Date?,
        @Expose val fromReg: String?,
        @Expose val toReg: String?,
        @Expose val qKind: QKind?,
        @Expose val planQPrev: Int?,
        @Expose val planQCurr: Int?,
        @Expose val factQPrev: Int?,
        @Expose val factQCurr: Int?,
        private val planCancelled: Boolean?,
        private val day: Date
) : Jsonable() {

    private val requestDate = Calendar.getInstance().time
    @Expose val isPlanNextDay: Boolean = !is24(planDate, day) && onNextDay(planDate, day)
    @Expose val isFactNextDay: Boolean = !is24(factDate, day) && onNextDay(factDate, day)
    @Expose val state: RegChangeState = initState()
    @Expose val planDateString = dateString(planDate)
    @Expose val factDateString = dateString(factDate)

    private fun dateString(date: Date?): String {
        if (date == null) return ""
        if (is24(date, day)) return TIME_NEXT_00
        return formatHHmm(date)
    }

    private fun initState(): RegChangeState {
        if (planCancelled == true)
            return RegChangeState.CANCELLED

        if (planDate != null
                && (factDate == null || hourDiff(factDate, planDate) > TIME_FAIL_H)
                && hourDiff(requestDate, planDate) > TIME_FAIL_H)
            return RegChangeState.FAILED

        if (factDate == null
                && planDate != null
                && hourDiff(planDate, requestDate) <= TIME_FAIL_H)
            return RegChangeState.AWAIT

        if (factDate != null
                && planDate == null)
            return RegChangeState.NOT_SCHEDULED

        if (factDate != null
                && planDate != null
                && hourDiff(factDate, planDate) > TIME_LATE_H
                && hourDiff(factDate, planDate) <= TIME_FAIL_H)
            return RegChangeState.LATE

        if (factDate != null
                && planDate != null
                && hourDiff(factDate, planDate) <= TIME_LATE_H)
            return RegChangeState.DONE

        return RegChangeState.UNKNOWN
    }
}