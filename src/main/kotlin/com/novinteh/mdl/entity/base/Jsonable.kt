package com.novinteh.mdl.entity.base

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.novinteh.mdl.entity.data.Aomn
import java.lang.reflect.Type

/**
 * @author Marinchenko V. A.
 */
open class Jsonable {

    fun toJson(): String? {
        val json = try {
            GsonBuilder().apply {
                excludeFieldsWithoutExposeAnnotation()
            }.create().toJson(this)
        } catch (e: Exception) {
            null
        }

        return checkNull(json)
    }

    companion object {
        @JvmStatic
        private fun checkNull(json: String?) =
                if (json == null || json == "null") null
                else json

        @JvmStatic
        fun isEmpty(json: String?) = json == "[]"

        @JvmStatic
        fun <J: Jsonable> from(json: String?, type: Type): J? = try {
            Gson().fromJson<J>(json, type) as J
        } catch (e: Exception) {
            null
        }

        @JvmStatic
        fun <J: Jsonable> listFrom(json: String?, type: Type): List<J?> = try {
            Gson().fromJson<List<J>>(json, type) as List<J?>
        } catch (e: Exception) {
            emptyList()
        }


        @JvmStatic
        fun <J : Jsonable> toJson(list: List<J?>?): String? {
            val json = try {
                GsonBuilder().apply {
                    excludeFieldsWithoutExposeAnnotation()
                }.create().toJson(list)
            } catch (e: Exception) {
                null
            }
            return checkNull(json)
        }
    }

}