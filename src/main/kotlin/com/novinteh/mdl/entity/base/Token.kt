package com.novinteh.mdl.entity.base

import com.google.gson.reflect.TypeToken
import com.novinteh.mdl.entity.data.Aomn
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.entity.data.Tehsect
import java.lang.reflect.Type
import kotlin.reflect.KClass

/**
 * @author Marinchenko V. A.
 */
class Token {

    companion object {

        val AOMN: Type = object : TypeToken<Aomn>(){}.type
        val AOMN_LIST: Type = object : TypeToken<List<Aomn>>(){}.type
        val REG_CHANGE: Type = object : TypeToken<RegChange>(){}.type
        val REG_CHANGE_LIST: Type = object : TypeToken<List<RegChange>>(){}.type
        val TEHSECT: Type = object : TypeToken<Tehsect>(){}.type
        val TEHSECT_LIST: Type = object : TypeToken<List<Tehsect>>(){}.type

    }

}