package com.novinteh.mdl.entity.util

import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * @author Marinchenko V. A.
 */
val hhmmDateFormat = SimpleDateFormat("HH:mm")
val ddMMDateFormat = SimpleDateFormat("dd.MM")
val ddMMyyyyDateFormat = SimpleDateFormat("dd.MM.yyyy")
val fullDateFormat = SimpleDateFormat("dd.MM.yyyy HH:mm")

fun formatHHmm(date: Date) = hhmmDateFormat.format(date)
fun parseHHmm(date: String) = hhmmDateFormat.parse(date)

fun formatddMM(date: Date) = ddMMDateFormat.format(date)
fun parseddMM(date: String) = ddMMDateFormat.parse(date)

fun formatddMMyyyy(date: Date) = ddMMyyyyDateFormat.format(date)
fun parseddMMyyyy(date: String) = ddMMyyyyDateFormat.parse(date)

fun formatFullDate(date: Date) = fullDateFormat.format(date)
fun parseFullDate(date: String) = fullDateFormat.parse(date)

fun hourDiff(date0: Date, date1: Date) =
        TimeUnit.MILLISECONDS.toHours(date0.time - date1.time).toInt()

fun onNextDay(date0: Date?, date1: Date?): Boolean {
    if (date0 == null || date1 == null) return false
    if (date0.before(date1)) return false
    if (hourDiff(date0, date1) >= 24) return true

    val day0 = Calendar.getInstance().apply { time = date0 }.get(Calendar.DATE)
    val day1 = Calendar.getInstance().apply { time = date1 }.get(Calendar.DATE)

    return day0 > day1
}

fun is24(date: Date?, compare: Date): Boolean {
    if (date == null) return false
    val cal = Calendar.getInstance().apply { time = date }
    return onNextDay(date, compare)
            && cal.get(Calendar.HOUR_OF_DAY) == 0
            && cal.get(Calendar.MINUTE) == 0
}