package json

import java.util.*
import kotlin.reflect.KClass

/**
 * @author Marinchenko V. A.
 */
open class DataGen {

    private val charPool : List<Char> = ('a'..'z') + ('A'..'Z') + ('0'..'9') + '_'
    private val random = Random()

    var nullProb = 0.2
    var maxStringLength = 50

    fun makeNull(nullProbability: Double = nullProb) = random.nextDouble() <= nullProbability

    fun length() = random.nextInt(maxStringLength)

    fun boolean() = random.nextBoolean()
    fun int() = random.nextInt()
    fun int(bound: Int) = random.nextInt(bound)
    fun uint(bound: Int) = Math.abs(random.nextInt(bound))
    fun long() = random.nextLong()
    fun double() = random.nextDouble()
    fun float() = random.nextFloat()

    fun date() = Date(Math.abs(long()))

    fun string() = (1..length())
            .map { random.nextInt(charPool.size) }
            .map(charPool::get)
            .joinToString("")


    fun <A: Any> newVal(kClass: KClass<out A>, nullable: Boolean = true): A? {
        if (nullable && makeNull(nullProb)) return null
        return types(kClass)
    }

    @Suppress("UNCHECKED_CAST")
    open fun <A: Any> types(kClass: KClass<out A>) = when(kClass) {
        Boolean::class -> boolean() as A
        Int::class -> int() as A
        Long::class -> long() as A
        Double::class -> double() as A
        Float::class -> float() as A
        String::class -> string() as A
        Date::class -> date() as A
        else -> null
    }
}
