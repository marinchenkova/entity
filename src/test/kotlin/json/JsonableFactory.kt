import com.google.gson.JsonParser
import com.google.gson.annotations.Expose
import com.novinteh.mdl.entity.base.Jsonable
import com.novinteh.mdl.entity.data.QKind
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.entity.data.RegChangeState
import com.novinteh.mdl.entity.data.Tehsect
import json.DataGen
import kotlin.reflect.KClass
import kotlin.reflect.full.declaredMemberProperties
import kotlin.reflect.full.memberProperties
import kotlin.reflect.full.primaryConstructor
import kotlin.reflect.jvm.javaField

/**
 * @author Marinchenko V. A.
 */
class JsonableFactory {

    var nullProb = 0.2
    var maxListSize = 100
    var maxStringLength = 20

    private val dataGen = object : DataGen() {
        @Suppress("UNCHECKED_CAST")
        override fun <A : Any> types(kClass: KClass<out A>): A? = when (kClass) {
            QKind::class -> qKind() as A
            RegChangeState::class -> regChangeState() as A
            Tehsect::class -> newJsonable(Tehsect::class, false) as A
            else -> super.types(kClass)
        }
    }

    init {
        dataGen.nullProb = nullProb
        dataGen.maxStringLength = maxStringLength
    }

    private fun qKind(): QKind = when (dataGen.int(3)) {
        0 -> QKind.FALL
        1 -> QKind.STABLE
        else -> QKind.GROWTH
    }

    private fun regChangeState(): RegChangeState = when (dataGen.int(7)) {
        0 -> RegChangeState.DONE
        1 -> RegChangeState.LATE
        2 -> RegChangeState.FAILED
        3 -> RegChangeState.AWAIT
        4 -> RegChangeState.CANCELLED
        5 -> RegChangeState.NOT_SCHEDULED
        else -> RegChangeState.UNKNOWN
    }

    fun <J: Jsonable> newJsonableList(kClass: KClass<out J>,
                                      nullable: Boolean = true,
                                      size: Int? = null): List<J?> {
        return ArrayList<J?>().apply {
            for (i in 0..(size ?: dataGen.uint(maxListSize))) {
                add(newJsonable(kClass, nullable))
            }
        }
    }

    fun <J: Jsonable> newJsonable(kClass: KClass<out J>,
                                  nullable: Boolean = true): J? {
        if (nullable && dataGen.makeNull()) return null
        val ctor = kClass.primaryConstructor ?: return null

        val args = ctor.parameters.map {
            dataGen.newVal(it.type.classifier as KClass<*>, it.type.isMarkedNullable)
        }.toTypedArray()

        return ctor.call(*args)
    }

    private fun readProperty(instance: Any, propertyName: String) = try {
        val i = instance.javaClass.kotlin.declaredMemberProperties.first {
            it.name == propertyName
        }.get(instance) ?: throw NullPointerException()

        when (i) {
            is Boolean,
            is Int,
            is Long,
            is Double,
            is Float -> i.toString()
            is Tehsect -> when (instance) {
                is RegChange -> newJson(instance.tehsect)
                else -> newJson(Tehsect::class, false)
            }
            else -> "\"$i\""
        }

    } catch (e: Exception) {
        null
    }

    fun newJson(jsonable: Jsonable?): String? {
        if (jsonable == null) return null

        val json = StringBuilder("{")
        json.append(
                jsonable::class.memberProperties
                        .filter { it.javaField?.getAnnotation(Expose::class.java) != null }
                        .filter { readProperty(jsonable, it.name) != null }
                        .joinToString(",") {
                            val value = readProperty(jsonable, it.name)
                            if (value != null) "\"${it.name}\":$value" else ""
                        }
        )
        json.append("}")

        return json.toString()
    }

    fun <J: Jsonable> newJson(kClass: KClass<out J>,
                              nullable: Boolean = true): String? {
        val jsonable = newJsonable(kClass, nullable) ?: return null
        return newJson(jsonable)
    }

    fun newJsonList(list: List<Jsonable?>): String? {
        val json = StringBuilder("[")
        json.append(list.joinToString(",") { newJson(it) ?: "null" })
        json.append("]")
        return json.toString()
    }

    fun <J: Jsonable> newJsonList(kClass: KClass<out J>,
                                  nullable: Boolean = true,
                                  size: Int? = null): String? {
        val list = ArrayList<Jsonable?>()
        for (i in 0..(size ?: dataGen.uint(maxListSize))) {
            list.add(newJsonable(kClass, nullable))
        }

        return newJsonList(list)
    }

    fun jsonEquals(json0: String?, json1: String?): Boolean {
        if (json0 == json1) return true
        if (json0 == null || json1 == null) return false

        val parser = JsonParser()
        val e0 = parser.parse(json0)
        val e1 = parser.parse(json1)

        return e0 == e1
    }

}
