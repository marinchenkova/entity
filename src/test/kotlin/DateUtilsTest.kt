import com.novinteh.mdl.entity.util.*
import org.junit.Test
import java.util.*

/**
 * @author Marinchenko V. A.
 */
class DateUtilsTest {

    @Test fun hourDiffTest() {
        val date0 = Calendar.getInstance()
        val date1 = Calendar.getInstance()
        date1.time = parseHHmm("00:00")

        date0.time = parseHHmm("00:59")
        assert(hourDiff(date0.time, date1.time) == 0)

        date0.time = parseHHmm("01:00")
        assert(hourDiff(date0.time, date1.time) == 1)

        date0.time = parseHHmm("01:59")
        assert(hourDiff(date0.time, date1.time) == 1)

        date0.time = parseHHmm("02:01")
        assert(hourDiff(date0.time, date1.time) == 2)
    }

    @Test fun onNextDayTest() {
        val date0 = Calendar.getInstance()
        val date1 = Calendar.getInstance()
        date1.time = parseFullDate("01.01.2000 00:00")

        date0.time = parseFullDate("02.01.2000 00:00")
        assert(onNextDay(date0.time, date1.time))

        date0.time = parseFullDate("02.01.2000 12:00")
        assert(onNextDay(date0.time, date1.time))

        date0.time = parseFullDate("31.12.1999 00:00")
        assert(!onNextDay(date0.time, date1.time))

        date0.time = parseFullDate("03.01.2000 00:00")
        assert(onNextDay(date0.time, date1.time))

        date1.time = parseFullDate("01.01.2000 13:00")
        date0.time = parseFullDate("01.01.2000 23:00")
        assert(!onNextDay(date0.time, date1.time))

        date1.time = parseFullDate("01.01.2000 23:00")
        date0.time = parseFullDate("02.01.2000 01:00")
        assert(onNextDay(date0.time, date1.time))
    }

    @Test fun is24Test() {
        val date = Calendar.getInstance()
        val compare = Calendar.getInstance()
        compare.time = parseFullDate("01.01.2000 00:00")

        date.time = parseFullDate("01.01.2000 00:00")
        assert(!is24(date.time, compare.time))

        date.time = parseFullDate("02.01.2000 00:00")
        assert(is24(date.time, compare.time))

        date.time = parseFullDate("02.01.2000 00:01")
        assert(!is24(date.time, compare.time))
    }
}