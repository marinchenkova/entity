import com.novinteh.mdl.entity.base.Jsonable
import com.novinteh.mdl.entity.base.Token
import com.novinteh.mdl.entity.data.Aomn
import com.novinteh.mdl.entity.data.RegChange
import com.novinteh.mdl.entity.data.Tehsect
import org.junit.Test
import kotlin.test.assertNull

/**
 * @author Marinchenko V. A.
 */
class JsonableTest {

    private val loops = 1000
    private val factory = JsonableFactory()
    private val types = listOf(
            Aomn::class,
            RegChange::class,
            Tehsect::class
    )

    @Test
    fun nullToFrom() {
        assertNull(Jsonable.from<Aomn>(null, Token.AOMN))
        assertNull(Jsonable.from<RegChange>(null, Token.REG_CHANGE))
        assertNull(Jsonable.from<Tehsect>(null, Token.TEHSECT))

        assertNull(Jsonable.toJson<Aomn>(null))
        assertNull(Jsonable.toJson<RegChange>(null))
        assertNull(Jsonable.toJson<Tehsect>(null))
    }

    @Test
    fun emptyToFrom() {
        assert(Jsonable.isEmpty(Jsonable.toJson(emptyList())))

        assertNull(Jsonable.from<Aomn>("", Token.AOMN))
        assertNull(Jsonable.from<RegChange>("", Token.REG_CHANGE))
        assertNull(Jsonable.from<Tehsect>("", Token.TEHSECT))

        assert(Jsonable.listFrom<Aomn>("", Token.AOMN_LIST).isEmpty())
        assert(Jsonable.listFrom<RegChange>("", Token.REG_CHANGE_LIST).isEmpty())
        assert(Jsonable.listFrom<Tehsect>("", Token.TEHSECT_LIST).isEmpty())
    }

    @Test
    fun wrongTokenEmptyList() {
        val aomnJson = factory.newJsonable(Aomn::class, false)?.toJson()
        val aomnListJson = Jsonable.toJson(factory.newJsonableList(Aomn::class, false, 50))

        assert(Jsonable.listFrom<Aomn>(aomnListJson, Token.AOMN).isEmpty())
        assert(Jsonable.listFrom<Aomn>(aomnJson, Token.AOMN_LIST).isEmpty())
        assert(Jsonable.listFrom<Aomn>(aomnListJson, Token.REG_CHANGE).isEmpty())
    }

    @Test
    fun wrongTokenReturnNull() {
        val aomnJson = factory.newJsonable(Aomn::class, false)?.toJson()
        assertNull(Jsonable.from<Aomn>(aomnJson, Token.AOMN_LIST))
        assertNull(Jsonable.from<Aomn>(aomnJson, Token.TEHSECT_LIST))
    }


    @Test(expected = ClassCastException::class)
    fun wrongTokenCastException() {
        val aomnJson = factory.newJsonable(Aomn::class, false)?.toJson()
        val aomnListJson = Jsonable.toJson(factory.newJsonableList(Aomn::class, false, 50))
        Jsonable.from<Aomn>(aomnJson, Token.REG_CHANGE) as Aomn
        Jsonable.listFrom<Aomn>(aomnListJson, Token.REG_CHANGE_LIST)[0] as Aomn
    }

    @Test
    fun oneToJson() {
        types.forEach {
            for (i in 0..loops) {
                val entity = factory.newJsonable(it, false)
                val actual = entity?.toJson()
                val expected = factory.newJson(entity)
                assert(factory.jsonEquals(expected, actual))
            }
        }
    }

    @Test
    fun listToJson() {
        types.forEach {
            for (i in 0..loops) {
                val list = factory.newJsonableList(it, true, 50)
                val actual = Jsonable.toJson(list)
                val expected = factory.newJsonList(list)
                assert(factory.jsonEquals(expected, actual))
            }
        }
    }

    @Test
    fun oneFromJson() {
        var expected: Jsonable?
        var expectedJson: String?
        var actual: Jsonable?
        var actualJson: String?

        for (i in 0..loops) {
            expected = factory.newJsonable(Aomn::class, false)
            expectedJson = factory.newJson(expected)
            actual = Jsonable.from<Aomn>(expectedJson, Token.AOMN)
            actualJson = actual?.toJson()
            assert(factory.jsonEquals(expectedJson, actualJson))

            expected = factory.newJsonable(RegChange::class, false)
            expectedJson = factory.newJson(expected)
            actual = Jsonable.from<RegChange>(expectedJson, Token.REG_CHANGE)
            actualJson = actual?.toJson()
            assert(factory.jsonEquals(expectedJson, actualJson))

            expected = factory.newJsonable(Tehsect::class, false)
            expectedJson = factory.newJson(expected)
            actual = Jsonable.from<Tehsect>(expectedJson, Token.TEHSECT)
            actualJson = actual?.toJson()
            assert(factory.jsonEquals(expectedJson, actualJson))
        }
    }

    @Test
    fun listFromJson() {
        var expected: List<Jsonable?>
        var expectedJson: String?
        var actual: List<Jsonable?>
        var actualJson: String?

        for (i in 0..loops) {
            expected = factory.newJsonableList(Aomn::class, true, 100)
            expectedJson = factory.newJsonList(expected)
            actual = Jsonable.listFrom<Aomn>(expectedJson, Token.AOMN_LIST)
            actualJson = Jsonable.toJson(actual)
            assert(factory.jsonEquals(expectedJson, actualJson))

            expected = factory.newJsonableList(RegChange::class, true, 100)
            expectedJson = factory.newJsonList(expected)
            actual = Jsonable.listFrom<RegChange>(expectedJson, Token.REG_CHANGE_LIST)
            actualJson = Jsonable.toJson(actual)
            assert(factory.jsonEquals(expectedJson, actualJson))

            expected = factory.newJsonableList(Tehsect::class, true, 100)
            expectedJson = factory.newJsonList(expected)
            actual = Jsonable.listFrom<Tehsect>(expectedJson, Token.TEHSECT_LIST)
            actualJson = Jsonable.toJson(actual)
            assert(factory.jsonEquals(expectedJson, actualJson))
        }
    }

}

